const express = require("express");
const path = require("path");

const app = express();

const port = 8000;

app.get("/", (req,res) => {
    res.sendFile(path.join(__dirname + "/views/course-365-new-1/index.html"));
})

app.use(express.static(__dirname + "/views/course-365-new-1"));

app.listen(port, () => {
    console.log("App listening on port: ", port );
});